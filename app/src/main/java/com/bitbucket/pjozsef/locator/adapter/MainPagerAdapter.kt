package com.bitbucket.pjozsef.locator.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.bitbucket.pjozsef.locator.LocatorApplication
import com.bitbucket.pjozsef.locator.R
import com.bitbucket.pjozsef.locator.fragment.MapsFragment
import com.bitbucket.pjozsef.locator.fragment.StatsFragment

class MainPagerAdapter(val fragmentManager: FragmentManager) :
        FragmentPagerAdapter(fragmentManager){

    override fun getCount() = 2

    override fun getItem(position: Int): Fragment {
        return when(position){
            0 -> StatsFragment()
            1 -> MapsFragment()
            else -> throw IllegalArgumentException("Position must be between 0 and 1, but was $position")
        }
    }

    override fun getPageTitle(position: Int): CharSequence {
        val resources = LocatorApplication.context.resources
        return when(position){
            0 -> resources.getString(R.string.fragment_title_stats)
            1 -> resources.getString(R.string.fragment_title_maps)
            else -> throw IllegalArgumentException("Position must be between 0 and 1, but was $position")
        }
    }
}