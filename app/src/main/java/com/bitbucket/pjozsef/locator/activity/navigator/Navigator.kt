package com.bitbucket.pjozsef.locator.activity.navigator


interface Navigator {
    fun goToSettings()
}